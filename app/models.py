from datetime import datetime
from app import db

# Model definitions
class TimestampMixin(object):
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)

class Subject(db.Model,TimestampMixin):
    __tablename__ = 'subjects'
    id = db.Column(db.Integer, primary_key=True)
    experimental_session = db.Column(db.Integer, unique=False, nullable=False)
    expectations_dax_bin_1 = db.Column(db.Integer, unique=False)
    expectations_dax_bin_2 = db.Column(db.Integer, unique=False)
    expectations_dax_bin_3 = db.Column(db.Integer, unique=False)
    expectations_dax_bin_4 = db.Column(db.Integer, unique=False)
    expectations_dax_bin_5 = db.Column(db.Integer, unique=False)
    expectations_dax_bin_6 = db.Column(db.Integer, unique=False)
    expectations_dax_bin_7 = db.Column(db.Integer, unique=False)

    payoff_treatment_id = db.Column(db.Integer, db.ForeignKey('treatments.id'))
    payoff_treatment = db.relationship('Treatment',
        backref=db.backref('subjects', lazy='dynamic'))

    payoff_financial_data_id = db.Column(db.Integer, db.ForeignKey('financial_data.id'))
    payoff_financial_data = db.relationship('FinancialData',
        backref=db.backref('subjects', lazy='dynamic'))

    def __init__(self, experimental_session):
        self.experimental_session = experimental_session

    def __repr__(self):
        return '<Subject %r>' % self.id

class Treatment(db.Model):
    __tablename__ = 'treatments'
    id = db.Column(db.Integer, primary_key=True)
    riskless_rate = db.Column(db.Float, unique=False, nullable=False)
    shifter_risky = db.Column(db.Float, unique=False, nullable=False)
    shifter_riskless = db.Column(db.Float, unique=False, nullable=False)
    endowment_liquid = db.Column(db.Float, unique=False, nullable=False)
    endowment_illiquid = db.Column(db.Float, unique=False, nullable=False)


class Observation(db.Model,TimestampMixin):
    __tablename__ = 'observations'
    id = db.Column(db.Integer, primary_key=True)
    inv_riskless = db.Column(db.Integer, nullable=False)
    inv_risky = db.Column(db.Integer, nullable=False)

    treatment_id = db.Column(db.Integer, db.ForeignKey('treatments.id'))
    treatment = db.relationship('Treatment',
        backref=db.backref('observations', lazy='dynamic'))

    subject_id = db.Column(db.Integer, db.ForeignKey('subjects.id'))
    subject = db.relationship('Subject',
        backref=db.backref('observations', lazy='dynamic'))

    step_id = db.Column(db.Integer, db.ForeignKey('steps.id'))
    step = db.relationship('Step',
        backref=db.backref('observations', lazy='dynamic'))

    def __init__(self, inv_riskless, inv_risky):
        self.inv_riskless = inv_riskless
        self.inv_risky = inv_risky

    def __repr__(self):
        return '<Observation %r>' % self.id

class Step(db.Model,TimestampMixin):
    __tablename__ = 'steps'
    id = db.Column(db.Integer, primary_key=True)
    screen = db.Column(db.Integer, unique=False, nullable=False)

    treatment_id = db.Column(db.Integer, db.ForeignKey('treatments.id'))
    treatment = db.relationship('Treatment',
        backref=db.backref('steps', lazy='dynamic'))

    subject_id = db.Column(db.Integer, db.ForeignKey('subjects.id'))
    subject = db.relationship('Subject',
        backref=db.backref('steps', lazy='dynamic'))

    def __init__(self, screen):
        self.screen = screen

    def __repr__(self):
        return '<Step %r>' % self.id

class FinancialData(db.Model):
    __tablename__ = 'financial_data'
    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer, unique=True)
    dax_nom = db.Column(db.Float)
    dax_real = db.Column(db.Float)