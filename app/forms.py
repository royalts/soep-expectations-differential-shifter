# -*- coding: utf-8 -*- 
from flask.ext.wtf import Form
from wtforms import IntegerField,ValidationError
from wtforms.widgets import HiddenInput
from wtforms.validators import InputRequired, NumberRange

class MyBaseForm(Form):
    class Meta:
        locales = ['de_DE', 'de']

class InvestmentForm(MyBaseForm):
    inv_riskless = IntegerField(u'In die Bundesanleihe',validators = [InputRequired(message = "Bitte geben Sie eine Zahl ein"),NumberRange(min=0, max=50000, message="Der Investitionsbetrag muss zwischen 0 und 50.000 EUR liegen")])
    inv_risky = IntegerField(u'In den Fonds',validators = [InputRequired(message = "Bitte geben Sie eine Zahl ein"),NumberRange(min=0, max=50000, message="Der Investitionsbetrag muss zwischen 0 und 50.000 EUR liegen")])

    def validate_inv_riskless(self, field):
        if self.inv_riskless.data is None or self.inv_risky.data is None:
            pass
        elif self.inv_riskless.data + self.inv_risky.data != 50000:
            raise ValidationError(message=u"Die Beträge ergeben zusammen nicht 50.000 EUR")

class BeliefForm(MyBaseForm):
    expectations_dax_bin_1 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])
    expectations_dax_bin_2 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])
    expectations_dax_bin_3 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])
    expectations_dax_bin_4 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])
    expectations_dax_bin_5 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])
    expectations_dax_bin_6 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])
    expectations_dax_bin_7 = IntegerField(widget=HiddenInput(), default=0, validators = [NumberRange(min=0, max=20)])

    def validate_expectations_dax_bin_1(self, field):
        if self.expectations_dax_bin_1.data + self.expectations_dax_bin_2.data + self.expectations_dax_bin_3.data + self.expectations_dax_bin_4.data + self.expectations_dax_bin_5.data + self.expectations_dax_bin_6.data + self.expectations_dax_bin_7.data != 20:
            raise ValidationError(message=u"Sie haben nicht alle 20 Blöcke verteilt")
