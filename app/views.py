from flask import render_template, flash, redirect, session, url_for, request, g
from app import app, db
from models import Subject, Treatment, Observation, Step, FinancialData
from forms import InvestmentForm, BeliefForm
from sqlalchemy import asc
import random

NUM_TREATMENTS = 8 # the number of treatments. FIXME: This is a bit of shortcut. Ideally we'd want to calculate this from the steps table
DEFAULT_SESSION = 2

# some global variables
@app.before_request
def before_request():  
    g.session_id = session['session_id'] if 'session_id' in session else DEFAULT_SESSION # the session. Change before each session. FIXME: this is a terrible variable name because the only thing that keeps it from being confused with the server's session is the capitalization

    # based on the session number, determine the version of the screens subjects should be presented with
    if g.session_id <=2:
        g.version = 1
        g.riskless_rate = 6

    else:
        g.version = 2
        g.riskless_rate = 4


# the context_processor below injects these global variables into the Jinja context (i.e. they're available to the templates without having to pass them in the return template() call)
@app.context_processor
def inject_globals():
    return dict(
        riskless_rate = g.riskless_rate,
        num_treatments = NUM_TREATMENTS
    )

# a template filter (i.e. a function available to Jinja) to format whole numbers in German format
@app.template_filter('format_number')
def format_number_filter(num):
    return '{:,.0f}'.format(num).replace(".","%").replace(",",".").replace("%",",")

# a template filter to format numbers in German format to 2 decimals.
@app.template_filter('format_decimal')
def format_decimal_filter(num):
    return '{:,.2f}'.format(num).replace(".","%").replace(",",".").replace("%",",")
# FIXME: write a single function that nests the two functions above

def render_screen(screen,version,**kwargs):
    return render_template('screen%d-%d.html' % (version,screen),**kwargs)

# courtesy of http://flask.pocoo.org/snippets/12/
def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Fehler bei der Eingabe im Feld  \"%s\": %s" % (
                getattr(form, field).label.text,
                error
            ),'form_error')

@app.route('/', methods=['GET'])
def index():
    if not 'subject_id' in session:
        return render_template('index.html')
    else:
        return redirect(url_for('experiment'))

@app.route('/experiment', methods=['GET', 'POST'])
def experiment():
    if not 'subject_id' in session:
        return redirect(url_for('index'))

    subject = Subject.query.get(int(session['subject_id']))
    steps = subject.steps.all() # FIXME: .order_by(asc(id))

    # if there are no more sequence steps, go to debrief
    if session['step_index']+1 > len(steps):
        return redirect(url_for('debrief'))

    current_step = steps[session['step_index']]
    
    if request.method == 'POST':
        if current_step.screen == 5:
            treatment = current_step.treatment
            current_step_index = session['step_index'] - 3
            form = InvestmentForm()
            if form.validate_on_submit():
                observation = Observation(inv_riskless = form.inv_riskless.data, inv_risky = form.inv_risky.data)
                observation.treatment = treatment
                observation.subject = subject
                db.session.add(observation)
                try:
                   db.session.commit()
                except Exception as e:
                   db.session.flush()
                else:
                    if app.debug: flash("DB commit successful")
                    session['step_index'] += 1
                    return redirect(url_for('experiment'))
            else:
                if app.debug: flash('Treatment %s' % treatment.id)
                flash_errors(form)
                return render_screen(current_step.screen, version=g.version, treatment=treatment, form=form, current_step_index=current_step_index)
        if current_step.screen == 7:
            form = BeliefForm()
            if form.validate_on_submit():
                subject.expectations_dax_bin_1 = form.expectations_dax_bin_1.data
                subject.expectations_dax_bin_2 = form.expectations_dax_bin_2.data
                subject.expectations_dax_bin_3 = form.expectations_dax_bin_3.data
                subject.expectations_dax_bin_4 = form.expectations_dax_bin_4.data
                subject.expectations_dax_bin_5 = form.expectations_dax_bin_5.data
                subject.expectations_dax_bin_6 = form.expectations_dax_bin_6.data
                subject.expectations_dax_bin_7 = form.expectations_dax_bin_7.data
                try:
                   db.session.commit()
                except Exception as e:
                   db.session.flush()
                else:
                    if app.debug: flash("DB commit successful")
                    session['step_index'] += 1
                    return redirect(url_for('experiment'))
            else:
                flash_errors(form)
                return render_screen(current_step.screen, version=g.version, form=form)
        else:
            # FIXME: dirty, dirty hack to skip a screen in version == 2
            if g.version == 2 and current_step.screen == 2:
                session['step_index'] += 2
            else:
                session['step_index'] += 1
            return redirect(url_for('experiment'))

    # FIXME: This isn't very DRY
    if request.method == 'GET':
        if current_step.screen == 5:
            treatment = current_step.treatment
            if app.debug: flash('Treatment %s' % treatment.id)
            form = InvestmentForm()
            current_step_index = session['step_index'] - 3
            return render_screen(current_step.screen, version=g.version, treatment=treatment, form=form, current_step_index=current_step_index)
        if current_step.screen == 7:
            form = BeliefForm()
            return render_screen(current_step.screen, version=g.version, form=form)
        else:
            return render_screen(current_step.screen, version=g.version)

@app.route('/debrief')
def debrief():
    subject = Subject.query.get(int(session['subject_id']))

    if subject.payoff_treatment is None:
        # select a random round to be paid out
        steps = subject.steps.filter(Step.treatment_id != None).all() # FIXME: .order_by(asc(id))
        random_step_index = random.randint(1, len(steps))
        random_step = steps[random_step_index-1]
        random_step_observation = subject.observations.filter(Observation.treatment_id == random_step.treatment.id).one()

        # select a random DAX return
        financial_data = FinancialData.query.all()
        random.shuffle(financial_data)
        random_year = financial_data[0]

        subject.payoff_treatment = random_step.treatment
        subject.payoff_financial_data = random_year

        # write the updated user to the db
        # FIXME: need error handling!
        db.session.commit()
    else:
        steps = subject.steps.filter(Step.treatment_id != None).all()
        random_step_index = [i for i in range(len(steps)) if steps[i].treatment_id == subject.payoff_treatment.id][0] + 1
        random_step = steps[random_step_index-1]
        random_step_observation = subject.observations.filter(Observation.treatment_id == random_step.treatment.id).one()
        random_year = subject.payoff_financial_data
    total_payout = random_step_observation.inv_riskless*(1+g.riskless_rate/100+random_step.treatment.shifter_riskless/100) + random_step_observation.inv_risky*(1+random_year.dax_nom+random_step.treatment.shifter_risky/100) + random_step.treatment.endowment_illiquid
    return render_template('debrief.html',random_year=random_year, random_step=random_step, random_step_index=random_step_index, random_step_observation = random_step_observation, total_payout=total_payout)

@app.route('/login', methods=['POST'])
def login():
    if not 'subject_id' in session:
        subject = Subject(g.session_id)
        db.session.add(subject)
        db.session.commit()
        session['subject_id'] = subject.id
        session['step_index'] = 0

        # Set up a sequence of screens that the experiment follows and save it to the db.
        for screen in range(1,8):
        # for screen in range(1,2):
            if screen == 5:
                # on the screen on which the treatments play a role...
                if g.version == 1:
                    treatments = Treatment.query.filter(Treatment.id.in_(range(1,9))).all()
                elif g.version == 2:
                    treatments = Treatment.query.filter(Treatment.id.in_(range(9,17))).all()
                # shuffle the treatments randomly
                random.shuffle(treatments)
                # and assign them
                # for treatment in treatments:
                for treatment in treatments:
                    step = Step(screen)
                    step.treatment = treatment
                    step.subject = subject
                    db.session.add(step)
            else:
                step = Step(screen)
                step.subject = subject
                db.session.add(step)
        db.session.commit() # save to DB
    return redirect(url_for('experiment'))

@app.route('/reset')
def reset():
    session.clear()
    return redirect(url_for('index'))

@app.route('/set_design/<design>')
def set_design(design):
    session['design'] = design
    return redirect(url_for('experiment'))

@app.route('/session/<int:session_id>')
def set_session(session_id):
    session.clear()
    session['session_id'] = session_id
    return redirect(url_for('index'))

