CSRF_ENABLED = True
SECRET_KEY = 'x\xf0G\x15\xc9\xd8\x04\x02o\x96/8\xc2\xb3\xa4\xcc\xab\xa5\xdd4\x01\xe0\xfd<'

import os
basedir = os.path.abspath(os.path.dirname(__file__))

if os.environ.get('HEROKU') is not None:
	DEBUG= False
else:
	DEBUG= True


if os.environ.get('DATABASE_URL') is None:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'db/data.db')
else:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_POOL_SIZE = 10
    
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')